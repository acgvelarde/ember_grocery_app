import Ember from 'ember';

export default Ember.Controller.extend({
    actions: {
        registerUser: function(userData){
            //var a = this.store.createRecord("user", {username: "text"});
            //a.save();
            $.ajax({
                data: {registration: userData},
                method: "POST",
                url: 'http://localhost:8000/registration'
            });
        }
    }
});