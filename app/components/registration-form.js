import Ember from 'ember';
import EmberValidations, { validator } from 'ember-validations';


var RegistrationFormValidation = Ember.Mixin.create(EmberValidations, {
    tagName: 'form',
    validationErrors: [],

    validations: {
        'username': {
            presence: { message: 'Username is required' }
        },
        'email': {
            presence: { message: 'Email is required' }
        },
        'password': {
            presence: { message: 'Password is required. ' },
            length: {minimum: 6, messages: {tooShort: "Password must be at least 6 alphanumeric characters."}}
        },
        'confirm_password': {
            inline: validator(function() {
                if (this.model.get('password') !== this.model.get('confirm_password')) {
                    return "Passwords don't match.";
                }
            })
        }
    },

    actions: {
        submitProfileForm: function(){

            var self = this;
            this.validate()
                .then(
                function() {
                    var userData = {
                        username: this.username,
                        email: this.email,
                        firstName: this.first_name,
                        lastName: this.last_name,
                        password: {
                            first: this.password,
                            second: this.password
                        }
                    };

                    console.log(userData);
                    this.sendAction('action', userData);
                }.bind(this)
            )
                .catch(function() {
                    self.set('hasError', true);
                });
        }
    }
});


export default Ember.Component.extend(RegistrationFormValidation);
